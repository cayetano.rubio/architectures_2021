import 'dart:developer';

import '../datasource/auth_datasource.dart';
import '../model/login_model.dart';
import '../../../core/error/failures.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';

abstract class LoginRepository {
  Future<Either<Failure, Login>> doLogin(String email, String password);
  Future<Either<Failure, bool>> getCookiesConfig();
  Future<Either<Failure, bool>> setCookiesConfig(bool conf);
}

class LoginRepositoryImpl implements LoginRepository {
  final AuthDataSource authDataSource;

  LoginRepositoryImpl({
    required this.authDataSource,
  });
  @override
  Future<Either<Failure, Login>> doLogin(String email, String password) {
    return _doLogin(email, password);
  }

  Future<Either<Failure, Login>> _doLogin(String email, String password) async {
    try {
      final remoteLogin = await authDataSource.doLogin(email, password, 'id');

      return Right(remoteLogin);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> getCookiesConfig() async {
    try {
      return const Right(true);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> setCookiesConfig(bool conf) async {
    try {
      return Right(conf);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
