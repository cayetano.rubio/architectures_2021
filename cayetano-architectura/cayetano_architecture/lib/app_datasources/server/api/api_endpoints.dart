import 'api_configuration.dart';

class ApiEndpoints {
  static const String register = ApiConfiguration.apiBase + "/auth/register";
  static const String login = ApiConfiguration.apiBase + "/auth/login";
  static const String logout = ApiConfiguration.apiBase + "/auth/logout";
  static const String renewToken =
      ApiConfiguration.apiBase + "/auth/renew-token";
  static const String rescuePassword =
      ApiConfiguration.apiBase + "/users/password/reset";
  static const String updateUser = ApiConfiguration.apiBase + "/users/{id}";
}
