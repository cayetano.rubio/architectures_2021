class ApiState {
  static const String production = "https://api.todotradicion.com/api/v1";
  static const String development = "https://api-dev.todotradicion.com/api/v1";
  static const String beta = "https://api-beta.todotradicion.com/api/v1";
  static const String debug = "http://localhost:8000/api/v1";
  static const String ngrok = "https://c44a-79-155-171-21.ngrok.io/api/v1";
}

class ApiConfiguration {
  static const String apiBase = ApiState.development;
  static const String registerToken =
      "c1b1e56d288adacfd1815bc7ee490d31a7d5c70e2396acd7094c3a4e8f04c6fa";
  static const String baseToken =
      "2eb7ecdd0da2cb28eec54beae226ddcefd1fb1e6e5cfdd7c5f8cb64a68df19b6";
}

class ApiHeaders {
  static final Map<String, String> nonTokenHeader = {
    'Content-Type': 'application/json',
    'accept': 'application/json'
  };
  static final Map<String, String> registerHeader = {
    'register-token': ApiConfiguration.registerToken,
    'Content-Type': 'application/json',
    'accept': 'application/json'
  };

  static final Map<String, String> nonLoggedHeader = {
    'accept': 'application/json',
    'authorization-token': ApiConfiguration.baseToken,
    'Content-Type': 'application/json'
  };

  static Map<String, String> loggedHeader(String bearerToken) {
    return {
      'Authorization': bearerToken,
      'Content-Type': 'application/json',
      'accept': 'application/json'
    };
  }
}
