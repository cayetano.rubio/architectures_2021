class User {
  final String? id;
  final String? name;
  final String? email;
  final String? password;

  final String? refreshToken;

  final String? role;
  final bool? active;

  final bool? validatedMail;
  final DateTime? birthday;
  final String? nationality;
  final String? residence;

  User({
    this.id,
    this.name,
    this.email,
    this.password,
    this.refreshToken,
    this.role,
    this.active,
    this.validatedMail,
    this.birthday,
    this.nationality,
    this.residence,
  });
}

class UserModel extends User {
  UserModel({
    String? id,
    String? name,
    String? email,
    String? password,
    String? refreshToken,
    String? role,
    bool? active,
    String? apiVersion,
    DateTime? birthday,
  }) : super(
          id: id,
          name: name,
          email: email,
          password: password,
          refreshToken: refreshToken,
          role: role,
          active: active,
          birthday: birthday,
        );

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json["_id"],
      name: json["name"],
      email: json["email"],
      password: json["password"],
      birthday:
          json["birthday"] == null ? null : DateTime.tryParse(json["birthday"]),
      refreshToken: json["refreshToken"],
      role: json["role"],
      active: json["active"] ?? false,
      apiVersion: json["apiVersion"],
    );
  }
}
