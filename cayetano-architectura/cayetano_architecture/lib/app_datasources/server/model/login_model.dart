
import 'user_model.dart';

class Login {
  final String? token;
  final User? user;
  final String? expiration;
  final String? refreshToken;

  Login({
    required this.token,
    required this.user,
    required this.expiration,
    required this.refreshToken,
  });
}


class LoginModel extends Login {
  LoginModel({
    required String? token,
    required UserModel? user,
    required String? expiration,
    required String? refreshToken,
  }) : super(
          token: token,
          user: user,
          expiration: expiration,
          refreshToken: refreshToken,
        );

  factory LoginModel.fromJson(Map<String, dynamic> json) {
    return LoginModel(
      token: json["token"],
      user: UserModel.fromJson(json["user"]),
      expiration: json["expiration"],
      refreshToken: json["refreshToken"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "token": token,
      "user": user,
      "expiration": expiration,
      "refreshToken": refreshToken
    };
  }
}
