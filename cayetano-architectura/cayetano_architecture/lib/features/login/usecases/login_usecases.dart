import '../../../app_datasources/server/model/login_model.dart';
import '../../../app_datasources/server/model/user_model.dart';
import '../../../app_datasources/server/repository/login_repository_impl.dart';
import '../../../core/error/failures.dart';
import '../../../core/usecase/usecase.dart';
import 'package:dartz/dartz.dart';

class LoginUseCase extends UseCase<Login, LoginParams> {
  final LoginRepositoryImpl repository;
  LoginUseCase({required this.repository});
  @override
  Future<Either<Failure, Login>> call(LoginParams params) async {
    return await repository.doLogin(params.email, params.password);
  }
}

class LoginParams {
  final String email;
  final String password;

  LoginParams({required this.email, required this.password});
}
