import 'package:dartz/dartz.dart';
import '../entities/login.dart';
import '../errors/failures.dart';
import '../repositories/login_repository.dart';
import 'usecase_definition.dart';



class DoLogin extends UseCase<Login, DoLoginParams> {
  final LoginRepository repository;

  DoLogin(this.repository);

  @override
  Future<Either<Failure, Login>> call(DoLoginParams params) async {
    return await repository.doLogin(params.email, params.password);
  }
}

class DoLoginParams extends NoParams {
  final String email;
  final String password;

  DoLoginParams({required this.email, required this.password});

  @override
  List<Object> get props => [email, password];
}
