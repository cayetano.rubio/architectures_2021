import 'package:flutter/material.dart';

class AppTextStyle {
  static const TextStyle titleTextStyle = TextStyle(fontSize: 45);
  static const TextStyle textTextStyle = TextStyle(fontSize: 14);
  static const TextStyle subTitleTextStyle = TextStyle(fontSize: 16);
  static const TextStyle hintTextStyle =
      TextStyle(fontSize: 16, color: Colors.grey);
}
