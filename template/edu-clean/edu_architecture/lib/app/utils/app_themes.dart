import 'package:flutter/material.dart';

class AppThemes {
  static const ButtonThemeData _buttonThemeData = ButtonThemeData(
      textTheme: ButtonTextTheme.normal,
      shape: StadiumBorder(),
      buttonColor: Color(0xFFE12D77),
      disabledColor: Colors.grey);

  static const AppBarTheme _appBarTheme = AppBarTheme(
      actionsIconTheme: IconThemeData(
    color: Colors.black,
  ));

  static const DialogTheme _dialogTheme = DialogTheme(
      backgroundColor: Colors.white,
      titleTextStyle: TextStyle(),
      contentTextStyle: TextStyle());

  static const BottomNavigationBarThemeData _bottomNavigationBar =
      BottomNavigationBarThemeData(
    backgroundColor: Colors.white,
    selectedIconTheme: IconThemeData(color: Colors.purple),
    unselectedIconTheme: IconThemeData(color: Colors.grey),
    selectedItemColor: Colors.purple,
    unselectedItemColor: Colors.grey,
    selectedLabelStyle: TextStyle(color: Colors.purple),
    unselectedLabelStyle: TextStyle(color: Colors.grey),
  );

  static final ThemeData appTheme = ThemeData(
          primaryColor: const Color(0xFFE12D77),
          highlightColor: Colors.transparent,
          splashColor: Colors.purple,
          dividerColor: Colors.transparent,
          brightness: Brightness.light,
          scaffoldBackgroundColor: Colors.white,
          buttonTheme: _buttonThemeData,
          appBarTheme: _appBarTheme,
          dialogTheme: _dialogTheme,
          bottomNavigationBarTheme: _bottomNavigationBar)
      .copyWith(
          colorScheme: appTheme.colorScheme
              .copyWith(secondary: const Color(0xFFE12D77)));
}
