class ApiState {
  static const String production = " ";
  static const String development = " ";
  static const String beta = " ";
}

class ApiConfiguration {
  static const String apiBase = ApiState.development;
  // static const String registerToken =
  //     " ";
  // static const String baseToken =
  //     " ";
}

class ApiHeaders {
  static final Map<String, String> nonTokenHeader = {
    'Content-Type': 'application/json',
    'accept': 'application/json'
  };
  // static final Map<String, String> registerHeader = {
  //   'register-token': ApiConfiguration.registerToken,
  //   'Content-Type': 'application/json',
  //   'accept': 'application/json'
  // };

  // static final Map<String, String> nonLoggedHeader = {
  //   'accept': 'application/json',
  //   'authorization-token': ApiConfiguration.baseToken,
  //   'Content-Type': 'application/json'
  // };

  static Map<String, String> loggedHeader(String bearerToken) {
    return {
      'Authorization': bearerToken,
      'Content-Type': 'application/json',
      'accept': 'application/json'
    };
  }
}

class ApiEndpoints {
  // static const String login = ApiConfiguration.apiBase + "/auth/login";

}
