import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'di/dependecy_injector.dart' as di;
import 'simple_bloc_observer.dart';

main() async {
  ///Dependecy injector inicialize
  await di.init();
  await di.sl.allReady();

  ///Fixed the rotation
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  ///Setting an observer that will be print every state change
  Bloc.observer = SimpleBlocObserver();

  runApp(MyApp());
}

//Change the name of the class for the name: Example: TemplateApp
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //This title must be change
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Material App Bar'),
        ),
        body: const Center(
          child: Text('Hello World'),
        ),
      ),
    );
  }
}
