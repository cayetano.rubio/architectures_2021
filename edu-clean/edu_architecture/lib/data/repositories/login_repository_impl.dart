import 'dart:developer';

import 'package:dartz/dartz.dart';
import '../datasources/auth_datasource.dart';
import '../models/login_model.dart';
import '../../domain/entities/login.dart';
import '../../domain/errors/exceptions.dart';
import '../../domain/errors/failures.dart';
import '../../domain/repositories/login_repository.dart';

class LoginRepositoryImpl implements LoginRepository {
  final AuthDataSource authDataSource;

  LoginRepositoryImpl({
    required this.authDataSource,
  });
  @override
  Future<Either<Failure, Login>> doLogin(String email, String password) {
    return _doLogin(email, password);
  }

  Future<Either<Failure, Login>> _doLogin(String email, String password) async {
    try {
      final remoteLogin = await authDataSource.doLogin(email, password, 'id');

      return Right(remoteLogin);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> setCookiesConfig(bool conf) async {
    try {
      return Right(conf);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> getCookiesConfig() {
    // TODO: implement getCookiesConfig
    throw UnimplementedError();
  }


}
