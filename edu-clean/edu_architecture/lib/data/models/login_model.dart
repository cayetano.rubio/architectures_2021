import '../../domain/entities/login.dart';
import 'user_model.dart';

class LoginModel extends Login {
  LoginModel({
    required String? token,
    required UserModel? user,
    required String? expiration,
    required String? refreshToken,
  }) : super(
          token: token,
          user: user,
          expiration: expiration,
          refreshToken: refreshToken,
        );

  factory LoginModel.fromJson(Map<String, dynamic> json) {
    return LoginModel(
      token: json["token"],
      user: UserModel.fromJson(json["user"]),
      expiration: json["expiration"],
      refreshToken: json["refreshToken"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "token": token,
      "user": user,
      "expiration": expiration,
      "refreshToken": refreshToken
    };
  }
}
