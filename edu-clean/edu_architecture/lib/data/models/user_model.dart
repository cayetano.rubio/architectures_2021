import '../../domain/entities/user.dart';

class UserModel extends User {
  UserModel({
    String? id,
    String? name,
    String? email,
    String? password,
    String? refreshToken,
    String? role,
    bool? active,
    String? apiVersion,
    DateTime? birthday,
  }) : super(
          id: id,
          name: name,
          email: email,
          password: password,
          refreshToken: refreshToken,
          role: role,
          active: active,
          birthday: birthday,
        );

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json["_id"],
      name: json["name"],
      email: json["email"],
      password: json["password"],
      birthday:
          json["birthday"] == null ? null : DateTime.tryParse(json["birthday"]),
      refreshToken: json["refreshToken"],
      role: json["role"],
      active: json["active"] ?? false,
      apiVersion: json["apiVersion"],
    );
  }
}
