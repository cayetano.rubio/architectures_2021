import 'dart:convert';
import 'dart:developer' as developer;

import '../models/login_model.dart';
import '../../domain/errors/exceptions.dart';
import 'package:http/http.dart' as http;
import '../data_constants.dart';
import '../models/user_model.dart';



abstract class AuthDataSource {
  Future<LoginModel> register(Map<String, dynamic> registerRequest);
  Future<UserModel> updateUser(
    String uderId,
    Map<String, dynamic> registerRequest,
    String token,
  );
  Future<LoginModel> doLogin(String email, String password, String uuid);
  Future<LoginModel> renewToken(String refreshToken);
}

class AuthDataSourceImpl implements AuthDataSource {
  final http.Client client;

  AuthDataSourceImpl({required this.client});

  @override
  Future<LoginModel> register(Map<String, dynamic> registerRequest) {
    return _doRegister(
      url: ApiEndpoints.register,
      registerRequest: registerRequest,
    );
  }

  Future<LoginModel> _doRegister({
    required String url,
    required Map<String, dynamic> registerRequest,
  }) async {
    var e = jsonEncode(
      registerRequest,
    );

    final response = await client.post(
      Uri.parse(url),
      headers: ApiHeaders.registerHeader,
      body: e,
    );

    if (response.statusCode >= 200 && response.statusCode <= 204) {
      return LoginModel.fromJson(json.decode(response.body));
    } else {
      developer.log(response.body, name: 'AuthDataSourceImpl');
      throw ServerException();
    }
  }

  @override
  Future<UserModel> updateUser(
    String uderId,
    Map<String, dynamic> registerRequest,
    String token,
  ) {
    return _doUpdateUser(
      uderId: uderId,
      url: ApiEndpoints.updateUser,
      registerRequest: registerRequest,
      token: token,
    );
  }

  Future<UserModel> _doUpdateUser({
    required String uderId,
    required String url,
    required Map<String, dynamic> registerRequest,
    required String token,
  }) async {
    String body = jsonEncode(registerRequest);

    final response = await client.patch(
      Uri.parse(url.replaceAll('{id}', uderId)),
      headers: ApiHeaders.loggedHeader("Bearer " + token),
      body: body,
    );

    if (response.statusCode >= 200 && response.statusCode <= 204) {
      return UserModel.fromJson(json.decode(response.body));
    } else {
      developer.log(response.body, name: 'UpdateUserDataSourceImpl');
      throw ServerException();
    }
  }

  @override
  Future<LoginModel> doLogin(String email, String password, String uuid) {
    return _getLogin(
      url: ApiEndpoints.login,
      body: json.encode({
        'email': email,
        'password': password,
        "uuid": uuid,
      }),
    );
  }

  Future<LoginModel> _getLogin({
    required String url,
    required String body,
  }) async {
    final response = await client.post(
      Uri.parse(url),
      headers: ApiHeaders.nonTokenHeader,
      body: body,
    );

    if (response.statusCode >= 200 && response.statusCode <= 204) {
      return LoginModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }

  @override
  Future<LoginModel> renewToken(String refreshToken) {
    return _renewToken(
      url: ApiEndpoints.renewToken,
      body: json.encode({
        'refreshToken': refreshToken,
      }),
    );
  }

  Future<LoginModel> _renewToken({
    required String url,
    required String body,
  }) async {
    final response = await client.post(
      Uri.parse(url),
      headers: ApiHeaders.nonLoggedHeader,
      body: body,
    );

    if (response.statusCode >= 200 && response.statusCode <= 204) {
      return LoginModel.fromJson(json.decode(response.body));
    } else {
      throw ServerException();
    }
  }
}
