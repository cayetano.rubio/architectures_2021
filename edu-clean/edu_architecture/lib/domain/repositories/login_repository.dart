import 'package:dartz/dartz.dart';
import '../entities/login.dart';
import '../errors/failures.dart';

abstract class LoginRepository {
  Future<Either<Failure, Login>> doLogin(String email, String password);
  Future<Either<Failure, bool>> getCookiesConfig();
  Future<Either<Failure, bool>> setCookiesConfig(bool conf);
}
