abstract class Failure {
  Failure([List properties = const <dynamic>[]]);

  @override
  List<Object> get props => props;
}

// general failures
class ServerFailure extends Failure {
  final String? msg, param;
  final int? statusCode;

  ServerFailure({this.msg, this.param, this.statusCode});

  @override
  List<Object> get props => [];
}

class ProductTransactionOutDateFailure extends Failure {
  final int? statusCode;

  ProductTransactionOutDateFailure({this.statusCode});

  @override
  List<Object> get props => [];
}

class InvalidParamsFailure extends Failure {
  final String errorCode;

  InvalidParamsFailure({this.errorCode = ""});
  @override
  List<Object> get props => [];
}

class NotLoggedInFailure extends Failure {
  @override
  List<Object> get props => [];
}

class TimeoutFailure extends Failure {
  @override
  List<Object> get props => [];
}

class LimitMessageFailure implements Failure {
  @override
  List<Object> get props => throw UnimplementedError();

  @override
  bool? get stringify => throw UnimplementedError();
}

class ContainsPersonalDataFailure extends Failure {
  @override
  List<Object> get props => [];
}
