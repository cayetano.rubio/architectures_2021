import 'user.dart';

class Login {
  final String? token;
  final User? user;
  final String? expiration;
  final String? refreshToken;

  Login({
    required this.token,
    required this.user,
    required this.expiration,
    required this.refreshToken,
  });
}
