class User {
  final String? id;
  final String? name;
  final String? email;
  final String? password;

  final String? refreshToken;

  final String? role;
  final bool? active;

  final bool? validatedMail;
  final DateTime? birthday;
  final String? nationality;
  final String? residence;

  User({
    this.id,
    this.name,
    this.email,
    this.password,
    this.refreshToken,
    this.role,
    this.active,
    this.validatedMail,
    this.birthday,
    this.nationality,
    this.residence,
  });
}
